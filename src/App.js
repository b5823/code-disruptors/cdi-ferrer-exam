import './App.css';

// [SECTION] DEPENDENCIES
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';

// [SECTION] FILE IMPORTS
import AppNavBar from './Components/AppNavBar';
import Home from './Pages/Home';
import Eren from './Components/Eren';
import Mikasa from './Components/Mikasa';
import Armin from './Components/Armin';


// [SECTION] RENDERS

function App() {
  return(
   <Router>
      <div>
        <AppNavBar />

        <Routes>
          <Route exact path="/" element={<Home/>} />
          <Route exact path="/eren" element={<Eren/>} />
          <Route exact path="/mikasa" element={<Mikasa/>} />
          <Route exact path="/armin" element={<Armin/>} />
        </Routes>

      </div>
    </Router>
  )
}

export default App;
