import img1 from '../images/img1.png';
import img2 from '../images/img2.png';
import img3 from '../images/img3.png';
import img4 from '../images/img4.png';
import img5 from '../images/img5.png';
import img6 from '../images/img6.png';

import {Link} from 'react-router-dom';

export default function Home(){
	return(

		<div className="container">
					<div className="row">
						<div className="col-md-12 my-4 text-center">
							<h3 className="main-heading">Shingeki no Kyojin</h3>
							<div className="underline mx-auto"></div>
							
						</div>

						<div className="col-md-4">
							<div className="card shadow">
								<img src={img1} className="w-100 border-bottom" alt="SnK" />
								<div className="card-body">
								<h4>Eren</h4>
								<Link to="/eren"><div className="btn btn-success shadow">Read More</div></Link>
								</div>
							</div>

						</div>

						<div className="col-md-4">
							<div className="card shadow">
								<img src={img2} className="w-100 border-bottom" alt="SnK" />
								<div className="card-body">
								<h4>Mikasa</h4>
								<Link to="/mikasa"><div className="btn btn-success shadow">Read More</div></Link>
								</div>
							</div>

						</div>

						<div className="col-md-4">
							<div className="card shadow">
								<img src={img3} className="w-100 border-bottom" alt="SnK" />
								<div className="card-body">
								<h4>Armin</h4>
								<Link to="/armin"><div className="btn btn-success shadow">Read More</div></Link>
								</div>
							</div>

						</div>
					</div>

					{/*SECTION 2*/}

					<div className="row">
						<div className="col-md-12 my-4">
							
						</div>

						<div className="col-md-4">
							<div className="card shadow">
								<img src={img4} className="w-100 border-bottom" alt="SnK" />
								<div className="card-body">
								<h4>Levi</h4>
								</div>
							</div>

						</div>

						<div className="col-md-4">
							<div className="card shadow">
								<img src={img5} className="w-100 border-bottom" alt="SnK" />
								<div className="card-body">
								<h4>Annie</h4>
								</div>
							</div>

						</div>

						<div className="col-md-4">
							<div className="card shadow">
								<img src={img6} className="w-100 border-bottom" alt="SnK" />
								<div className="card-body">
								<h4>Erwin</h4>
								</div>
							</div>

						</div>
					</div>

					{/*SECTION 3*/}

					<div className="row mb-5">
						<div className="col-md-12 my-4">
							
						</div>

						<div className="col-md-4">
							<div className="card shadow">
								<img src={img1} className="w-100 border-bottom" alt="SnK" />
								<div className="card-body">
								<h4>Eren</h4>
								</div>
							</div>

						</div>

						<div className="col-md-4">
							<div className="card shadow">
								<img src={img2} className="w-100 border-bottom" alt="SnK" />
								<div className="card-body">
								<h4>Mikasa</h4>
								</div>
							</div>

						</div>

						<div className="col-md-4">
							<div className="card shadow">
								<img src={img3} className="w-100 border-bottom" alt="SnK" />
								<div className="card-body">
								<h4>Armin</h4>
								</div>
							</div>

						</div>
					</div>

		</div>

	)
}
