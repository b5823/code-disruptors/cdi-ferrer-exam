import eren from '../images/eren.png';

import {Link} from 'react-router-dom';

export default function Eren(){
	return(

		<div className="row container-fluid">
			<div className="col-md-12 my-4">
				
			</div>

			<div className="col-md-8">
				<img src={eren} className="w-100 border-bottom" alt="SnK" />
			</div>

			<div className="col-md-4">

					<div className="card-body">

					<h5>Full Name:</h5>
					<p>Eren Yeager</p>

					<h5>Age:</h5>
					<p>18</p>

					<h5>Gender:</h5>
					<p>Male</p>

					<h5>Description:</h5>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</p>

					<Link to="/"><div className="btn btn-warning shadow">Go back to gallery</div></Link>

					</div>

			</div>
		</div>

	)
}
