import {Navbar, Nav} from 'react-bootstrap';

import {Link} from 'react-router-dom';

export default function AppNavBar(){
	return(
		<Navbar bg="dark" variant="dark" className="sticky-top">
		        <div className="container">
		        	<Navbar.Brand as={Link} to = "/">
 		            CDI Technical Exam
          			</Navbar.Brand>
		          <Nav className="ms-auto">
		       
		            <Nav.Link as={Link} to = "/">Home</Nav.Link>
		            <Nav.Link as={Link} to = "/about">About Us</Nav.Link>
		            <Nav.Link as={Link} to = "/contact">Contact Us</Nav.Link>

		          </Nav>
		        </div>
		      </Navbar>
	)
}
